# NOX Materials

## What are NOX Materials

This is an archive of textures made for the 2014 open source release of the NOX 3d renderer.


## Open source

According to Evermotion in a post on 2014-08-26 at 12:00:

    After years of development we decided to give NOX for the community.

    NOX physically based renderer, fully integrated with Blender and 3ds Max (and with C4D support) is now Open Source. We release it on Apache license - free to commercial use and modifications.

    You can freely improve and modify this render engine, integrate it with any 3d software, write plugins for NOX, use it in your commercial works and / or sell it. The possibilities are endless and depend only on you.

    Main features of NOX:

    - physical based engine
    - enhanced postprocesing
    - rendering to layers
    - real and fake dof
    - instancing and displacement
    - subsurface scattering

Read the original post at https://evermotion.org/articles/show/8845/nox-renderer-is-now-open-source-software-
